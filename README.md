### About
This project is a Flask backend which serves two endpoints for accessing data on Star Wars starships.

### Setup 
<h6> tested on Ubuntu 20.04 </h6>

1) Clone the repository

    `$ git clone https://gitlab.com/Hameem1/altagram-assessment.git`


2) The server can be run in two ways; using Python3, or by using Docker. 
   In both cases, the run.py is the application's entrypoint.
   Docker is recommended as it will get rid of any dependency issues 
   and uses Gunicorn as the Web Server Gateway Interface (WSGI).

- Using Python3 (with a virtual environment)
  
  ```
  (linux)
  $ cd altagram-assessment
  $ python3 -m venv venv
  $ source venv/bin/activate
  $ pip install -r requirements.txt
  $ python run.py
  
  (windows 10 with git-bash) [untested]
  $ cd altagram-assessment
  $ python -m venv venv
  $ source venv/Scripts/activate
  $ pip install -r requirements.txt
  $ python run.py
  ```

- Using Docker [(to install Docker)](#docker-installation)

  ```
  $ cd altagram-assessment
  $ docker build -t starships . && docker run --name starship_server -p 8000:8000 starships
  ```

    Using either method will run the server on **localhost** port **8000**.  


### Usage

- The Flask server handles HTTP GET requests on the following endpoints:

  ```
  - /api/starships [GET]
  - /api/starships/<hyperdrive_rating> [GET]
  ```
  
  
- **/api/starships** returns a complete list of starships along with their hyperdrive ratings. 
  It also accepts the **sort** parameter as a query string argument. 
  The **sort** parameter can take on either 'ascending' or 'descending' as the parameter values, 
  with 'ascending' being the default behavior.
  For example;
  
    - http://localhost:8000/api/starships
    - http://localhost:8000/api/starships?sort=descending  
    

- **/api/starships/<hyperdrive_rating>** will return all the starships with the specified hyperdrive rating. 
  An HTTP 400 will be returned if the provided hyperdrive value is not valid. 
  Here's an example of a valid request for this endpoint;

    - http://localhost:8000/api/starships/3
    - http://localhost:8000/api/starships/4.0  
    

---

#### Docker Installation
   
   ```
   $ sudo apt update && sudo apt upgrade
   $ sudo apt install docker.io
   $ sudo systemctl start docker
   $ sudo systemctl enable docker
   # Verify install
   $ docker run hello-world
   # if getting permission error
   $ sudo usermod -a -G docker $USER
   # and reboot
   ```
