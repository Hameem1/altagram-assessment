"""Contains functionality for fetching the required json data."""

import json
import requests
import logging
from pathlib import Path

# configuring data path
data_dir = Path('data')
data_filename = 'starships.json'
data_path = data_dir / data_filename

# configuring logger
formatter = logging.Formatter(fmt='%(levelname)s | %(asctime)s | %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
console_handler.setFormatter(formatter)
logger = logging.getLogger('logger')
logger.addHandler(console_handler)
logger.setLevel(logging.DEBUG)


def fetch_starships_data():
    """Fetches and stores data for all Star Wars Starships from an external API (swapi)."""

    # external API endpoint
    endpoint = 'https://swapi.dev/api/starships/'
    # fetching all the starships data from the API
    starships = []

    try:
        while True:
            req = requests.get(endpoint)
            r_next = req.json()['next']
            data = req.json()['results']
            if data:
                starships.extend(data)
            if r_next:
                endpoint = r_next
            else:
                break

        #  saving the fetched data as json
        if starships:
            with open(data_path, 'w') as f:
                f.write(json.dumps(starships, indent=2))
    except Exception as e:
        logger.exception(e)
    else:
        logger.info('Data for starships fetched and saved from external API!\n')


if __name__ == '__main__':
    fetch_starships_data()
