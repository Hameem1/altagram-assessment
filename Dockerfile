FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update -y && apt upgrade -y
RUN apt update -y && apt install -y apt-utils python3-pip python3-dev python3-venv
RUN pip3 install --upgrade pip
# Copying necessary files
RUN mkdir /altagram-assessment
WORKDIR /altagram-assessment
COPY ./requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .
CMD ["gunicorn", "-b", "0.0.0.0:8000", "run:app", "--log-level", "DEBUG"]
