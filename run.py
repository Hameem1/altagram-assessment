"""
Run this module directly using Python3, or use it to provide gunicorn with the 'app' callable.
"""

from app import app

if __name__ == '__main__':
    app.run(port=8000, debug=False)
