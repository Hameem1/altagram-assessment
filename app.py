"""Contains the Flask server's initialization and endpoints."""

import json
from flask import Flask, request
from get_data import data_path, fetch_starships_data, logger

# Setting up flask app
app = Flask(__name__)

# Fetching the starships data that needs to be served
if not data_path.exists():
    fetch_starships_data()


@app.route('/api/starships', methods=['GET'])
@app.route('/api/starships/<hyperdrive_rating>', methods=['GET'])
def get_starships(hyperdrive_rating=None):
    """
    Returns a list of Star Wars starships ordered by hyperdrive rating.
    Also, accepts a 'hyperdrive_rating' and returns the starships with a matching rating.
    Accepts query parameter : 'sort' ['ascending', 'descending'] : default = 'ascending'

    Returns:
          {msg, starships}, [200, 400, 500]
    """

    #  loading in the starships data
    try:
        with open(data_path, 'r') as f:
            starships = json.loads(f.read())
    except Exception as e:
        logger.error(f"Unable to open the file at path : {data_path.as_posix()}")
        logger.exception(e)

    default_sort = "ascending"
    sort_options = ["ascending", "descending"]
    sort_arg = request.args.get('sort', default_sort)
    sort_arg = sort_arg if sort_arg in sort_options else default_sort

    try:
        starships = [dict(name=s['name'], hyperdrive_rating=s['hyperdrive_rating']) for s in starships]
        # if a hyperdrive rating is specified in the request
        if hyperdrive_rating:
            filtered_starships = [s for s in starships if s['hyperdrive_rating'] == str(float(hyperdrive_rating))]
            # if no data found for the provided hyperdrive rating
            if not filtered_starships:
                return {'msg': f'No starships with a hyperdrive rating of {hyperdrive_rating}.'}, 400
            return {'msg': 'Success', "starships": filtered_starships}, 200

        # sorting the data for the starships
        starships.sort(key=lambda s: s['hyperdrive_rating'], reverse=(sort_arg == 'descending'))
        return {'msg': 'Success', "starships": starships}, 200

    except Exception as e:
        logger.exception(e)
        return {"msg": "Encountered an unknown server error!"}, 500
